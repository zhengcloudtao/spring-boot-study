package boot.controller;

import boot.bean.Car;
import boot.bean.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//@ResponseBody
//@Controller
@Slf4j
@RestController
public class HelloController {
    @Autowired
    Car car;
    @Autowired
    Person person;

    @RequestMapping("/car")
    public Car cat() {
        return car;
    }

    //@ResponseBody
    @RequestMapping("/hello")
    public String handler01() {
        log.info("/hello请求");
        return "HelloWorld SpringBoot2" + "你好";
    }
    @RequestMapping("/person")
    public Person person(){
        return person;
    }
}

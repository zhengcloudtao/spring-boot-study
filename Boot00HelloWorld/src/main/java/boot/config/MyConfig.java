package boot.config;

import boot.bean.Car;
import boot.bean.Person;
import boot.bean.Pet;
import boot.bean.User;
import ch.qos.logback.core.db.DBHelper;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;

/**
 * 配置类
 */
//@ConditionalOnBean(name="tom")
@Import({User.class, DBHelper.class})
@Configuration
@ImportResource("classpath:beans.xml")
@EnableConfigurationProperties({Car.class, Person.class})
//1.开启car配置绑定功能
//2.把car这个组件自动注册到容器中
public class MyConfig {
    //@ConditionalOnBean(name="tom")    条件
    @Bean
    public User user01(){
        User zhangSan= new User("zhangSan",18);
        zhangSan.setPet(tomcatPet());
        return new User("zhangSan",18);
    }

    @Bean("tomcatCloud")
    public Pet tomcatPet(){
        return new Pet("tomcatCloud");
    }

}

package boot.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
public class User {
    private String name;
    private int age;
    public Pet pet;

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }
}

package boot;

import boot.bean.Pet;
import boot.bean.User;
import boot.config.MyConfig;
import ch.qos.logback.core.db.DBHelper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootConfiguration
@EnableAutoConfiguration
@ComponentScan("boot")
//@SpringBootApplication
public class MainApplication {

    public static void main(String[] args) {
        //1、返回我们IOC容器
        ConfigurableApplicationContext run = SpringApplication.run(MainApplication.class, args);
        //2、查看容器里面的组件
        String[] names = run.getBeanDefinitionNames();
        for (String name : names) {
            System.out.println(name);
        }
        //3、从容器中获取组件
        Pet tomo1 = run.getBean("tomcatCloud", Pet.class);
        System.out.println(tomo1.toString());
        //MyConfig也是容器中组件
        MyConfig bean = run.getBean(MyConfig.class);
        System.out.println(bean);
        User user = bean.user01();
        User user1 = bean.user01();
        System.out.println(user==user1);
        User user01 = run.getBean("user01", User.class);
        Pet tomcatCloud = run.getBean("tomcatCloud", Pet.class);
        System.out.println(user01.getPet()==tomcatCloud);
        //@Import
        String[] beanNamesForType = run.getBeanNamesForType(User.class);
        for (String s : beanNamesForType) {
            System.out.println(s);
        }
        DBHelper bean1 = run.getBean(DBHelper.class);
        System.out.println(bean1);
        //@ConditionalOnBean
        System.out.println("容器中Tom组件:"+run.containsBean("tom"));
        System.out.println("容器中user01组件:"+run.containsBean("user01"));
        //@ImportResource
        System.out.println("容器中haha组件:"+run.containsBean("haha"));
        System.out.println("容器中hehe组件:"+run.containsBean("hehe"));
    }

}

package com.cloud.boot02admin;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.concurrent.TimeUnit;

//@SpringBootTest
public class Junit5Test {


    @Autowired
    JdbcTemplate jdbcTemplate;

    @DisplayName("测试前置条件")
    @Test
    void testAssumptions(){
        Assumptions.assumeTrue(true,"结果不是true");
        System.out.println("testAssumptions");
    }


    @DisplayName("测试简单断言")
    @Test
    void testSimpleAssertions() {
        int cal = cal(2, 3);
        Assertions.assertEquals(5, cal);
        //Assertions.assertSame(new Object(), new Object());
    }

    @DisplayName("测试断言数组")
    @Test
    void array() {
        Assertions.assertArrayEquals(new int[]{1, 2}, new int[]{1, 2});
    }

    @DisplayName("测试断言异常")
    @Test
    void testException() {
        Assertions.assertThrows(ArithmeticException.class, () -> {
            int i = 10 / 0;
        }, "业务逻辑");
    }
    @DisplayName("测试快速失败")
    @Test
    void testFail() {
       if(2==2){
           Assertions.fail("测试失败");
       }
    }
    @DisplayName("组合断言")
    @Test
    void all() {
        Assertions.assertAll("test", () -> Assertions.assertTrue(true && true), () -> Assertions.assertEquals(1, 1));
    }

    int cal(int i, int j) {
        return i + j;
    }

    @DisplayName("测试DisplayName")
    @Test
    void testDisplayName() {
        System.out.println(1);
        System.out.println(jdbcTemplate);
    }

    @Disabled
    @DisplayName("测试方法2")
    @Test
    void test2() {
        System.out.println(1);
    }

    @RepeatedTest(5)
    @Test
    void test3() {
        System.out.println(5);
    }

    /**
     * java.lang.InterruptedException: sleep interrupted
     */
    @Timeout(value = 500, unit = TimeUnit.MILLISECONDS)
    @Test
    void testTimeout() {
        try {
            Thread.sleep(400);
            System.out.println("Timeout");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @BeforeEach
    @Test
    void testBeforeEach() {
        System.out.println("测试就要开始了!");
    }

    @AfterEach
    @Test
    void testAfterEach() {
        System.out.println("测试结束了!");
    }

    @BeforeAll
    @Test
    static void testBeforeAll() {
        System.out.println("所有测试要开始了!");
    }

    @AfterAll
    @Test
    static void testAfterAll() {
        System.out.println("所有测试结束了!");
    }
}

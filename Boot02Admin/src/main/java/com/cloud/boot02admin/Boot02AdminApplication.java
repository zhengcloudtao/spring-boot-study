package com.cloud.boot02admin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
@MapperScan("com.cloud.boot02admin.mapper")
@ServletComponentScan(basePackages = "com.cloud.boot02admin")
@SpringBootApplication
public class Boot02AdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(Boot02AdminApplication.class, args);
    }

}

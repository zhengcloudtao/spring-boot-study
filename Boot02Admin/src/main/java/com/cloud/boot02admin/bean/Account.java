package com.cloud.boot02admin.bean;

import lombok.Data;

@Data
public class Account {
    private  Long id;
    private  String userId;
    private  Integer money;
}

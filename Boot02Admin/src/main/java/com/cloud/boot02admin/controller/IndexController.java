package com.cloud.boot02admin.controller;

import com.cloud.boot02admin.bean.Account;
import com.cloud.boot02admin.bean.City;
import com.cloud.boot02admin.bean.User;
import com.cloud.boot02admin.service.AccountService;
import com.cloud.boot02admin.service.CityService;
import com.cloud.boot02admin.service.impl.AccountServiceImpl;
import com.cloud.boot02admin.service.impl.CityServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.thymeleaf.util.StringUtils;

import javax.servlet.http.HttpSession;

@Slf4j
@Controller
public class IndexController {
    @GetMapping(value = {"/", "/login"})
    public String loginPage() {
        return "login";
    }

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    AccountService accountService;
    @Autowired
    CityService cityService;
    @Autowired
    StringRedisTemplate stringRedisTemplate;
    @ResponseBody
    @PostMapping("/city")
    public City saveCity(City city) {
        cityService.saveCity(city);
        return city;
    }

    @ResponseBody
    @GetMapping("/city")
    public City getCityById(@RequestParam("id") Long id) {
        return cityService.getById(id);
    }

    @ResponseBody
    @GetMapping("/acct")
    public Account getById(@RequestParam("id") Long id) {
        return accountService.getAccount(id);
    }

    @ResponseBody
    @GetMapping("/sql")
    public String queryFormDb() {
        String s = jdbcTemplate.queryForObject("select count(*) from t_account", String.class);
        log.info(s);
        return s;
    }

    @PostMapping("/login")
    public String main(User user, HttpSession session, Model model) {
        if (!StringUtils.isEmpty(user.getUsername()) && !StringUtils.isEmpty(user.getPassword())) {
            session.setAttribute("loginUser", user);
            return "redirect:/main";
        } else {
            model.addAttribute("msg", "账号密码错误!");
            return "login";
        }

    }

    @GetMapping("/main")
    public String mainPage(HttpSession session, Model model) {
        log.info("当前方法是:main()页面执行");
        Object loginUser = session.getAttribute("loginUser");
//        if (loginUser != null) {
//            return "main";
//        } else {
//            model.addAttribute("msg", "未登录");
//            return "login";
//        }
        ValueOperations<String, String> stringStringValueOperations = stringRedisTemplate.opsForValue();
        String s = stringStringValueOperations.get("/main.html");
        String s1 = stringStringValueOperations.get("/sql");

        model.addAttribute("mainCount",s);
        model.addAttribute("sqlCount",s1);
        return "main";
    }
}

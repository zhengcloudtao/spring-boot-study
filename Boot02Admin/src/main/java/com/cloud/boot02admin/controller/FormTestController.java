package com.cloud.boot02admin.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;

@Controller
@Slf4j
public class FormTestController {
    @GetMapping("/form_layouts")
    public String form_layouts() {
        return "/form/form_layouts";
    }

    @PostMapping("/upload")
    public String upload(@RequestParam("email") String email,
                         @RequestParam("username") String username,
                         @RequestParam("headerImg") MultipartFile headerImg,
                         @RequestParam("photos") MultipartFile[] photos,
                         HttpServletRequest request) {
        System.out.println(request.getServletContext().getRealPath("/"));
        String path =request.getServletContext().getRealPath("/");
        log.info("上传信息:email:" + email + ",username:" + username + ",headImg:" + headerImg.getSize() + ",photos:" + photos.length);
        if (!headerImg.isEmpty()) {
            try {
                String originalFilename = headerImg.getOriginalFilename();
                File file = new File(path + originalFilename);
                headerImg.transferTo(file);
                log.info("上传成功:"+file.toPath());

            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        if (photos.length > 0) {
            for (MultipartFile photo : photos) {
                if (!photo.isEmpty()) {
                    try {
                        String originalFilename = photo.getOriginalFilename();
                        photo.transferTo(new File(path + originalFilename));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return "main";
    }
}

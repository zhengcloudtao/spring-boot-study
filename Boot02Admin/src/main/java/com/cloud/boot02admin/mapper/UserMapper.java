package com.cloud.boot02admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cloud.boot02admin.bean.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<User> {

}

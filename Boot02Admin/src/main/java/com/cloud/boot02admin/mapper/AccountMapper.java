package com.cloud.boot02admin.mapper;

import com.cloud.boot02admin.bean.Account;
import org.apache.ibatis.annotations.Mapper;

//@Mapper
public interface AccountMapper {
    public Account getAccount(Long id);
}

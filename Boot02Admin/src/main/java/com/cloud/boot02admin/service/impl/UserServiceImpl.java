package com.cloud.boot02admin.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cloud.boot02admin.bean.User;
import com.cloud.boot02admin.mapper.UserMapper;
import com.cloud.boot02admin.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {


}

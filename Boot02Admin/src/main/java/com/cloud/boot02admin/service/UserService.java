package com.cloud.boot02admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cloud.boot02admin.bean.User;

/**
 *  Service 的CRUD也不用写了
 */
public interface UserService extends IService<User> {

}

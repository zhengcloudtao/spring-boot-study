package com.cloud.boot02admin.service;

import com.cloud.boot02admin.bean.City;
import com.cloud.boot02admin.mapper.CityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service

public interface CityService {
    City getById(Long id);
    void saveCity(City city);
}

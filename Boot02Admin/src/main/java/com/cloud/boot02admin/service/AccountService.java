package com.cloud.boot02admin.service;

import com.cloud.boot02admin.bean.Account;
import com.cloud.boot02admin.mapper.AccountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public interface AccountService {

    Account getAccount(Long id);
}


package com.cloud.boot02admin.service.impl;

import com.cloud.boot02admin.bean.City;
import com.cloud.boot02admin.mapper.CityMapper;
import com.cloud.boot02admin.service.CityService;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class CityServiceImpl implements CityService {

    @Autowired
    CityMapper cityMapper;
    Counter counter;
    public CityServiceImpl(MeterRegistry meterRegistry){
        counter = meterRegistry.counter("cityService.saveCity.count");

    }

    @Override
    public City getById(Long id){
        return cityMapper.getById(id);
    }

    @Override
    public void saveCity(City city) {
        counter.increment();
        cityMapper.insert(city);

    }
}


package com.cloud.boot02admin.service.impl;

import com.cloud.boot02admin.bean.Account;
import com.cloud.boot02admin.mapper.AccountMapper;
import com.cloud.boot02admin.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountMapper accountMapper;

    @Override
    public Account getAccount(Long id){
        return accountMapper.getAccount(id);
    }
}

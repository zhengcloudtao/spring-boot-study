package com.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Boot01WebApplication {

    public static void main(String[] args) {
        SpringApplication.run(Boot01WebApplication.class, args);
    }

}

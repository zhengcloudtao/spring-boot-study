package com.cloud.controller;

import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class ParamterTestController {
    @GetMapping("/car/{id}/owner/{username}")
    public Map<String ,Object> getCar(@PathVariable("id") Integer id,
                                      @PathVariable("username")String username,
                                      @PathVariable Map<String ,String> pv,
                                      @RequestHeader("User-Agent") String userAgent,
                                      @RequestHeader Map<String,String> headers,
                                      @RequestParam("age") Integer age,
                                      @RequestParam("inters") List<String> inters,
                                      @RequestParam Map<String,String> params,
                                      @CookieValue("username-localhost-8888") String usernameCookie,
                                      @CookieValue("username-localhost-8888") Cookie cookie
    ){
        Map<String,Object> map=new HashMap<>();
        map.put("id",id);
        map.put("username",username);
        map.put("pv",pv);

        map.put("userAgent",userAgent);
        map.put("headers",headers);

        map.put("age",age);
        map.put("inters",inters);
        map.put("params",params);

        map.put("username",usernameCookie);
        System.out.println(cookie.getName()+"<==>"+cookie.getValue());
        return map;
    }
    @PostMapping("/save")
    public Map postMethod(@RequestBody  String content){
        Map<String,Object> map=new HashMap<>();
        map.put("content",content);
        return map;
    }

    /**
     * SpringBoot默认禁用矩阵变量
     * remove
     * @param low
     * @param brand
     * @return
     */
    @GetMapping("/cars/{path}")
    public Map carsSell(@MatrixVariable("low") Integer low,@MatrixVariable("brand" )List<String> brand,@PathVariable("path") String path){
        Map<String,Object> map=new HashMap<>();
        map.put("low",low);
        map.put("brand",brand);
        map.put("path",path);
        return  map;
    }
    @GetMapping("/boss/{bossId}/{empId}")
    public Map boss(@MatrixVariable(value = "age",pathVar = "bossId") Integer bossAge,
                    @MatrixVariable(value = "age",pathVar = "empId") Integer empAge){
        Map<String,Object> map=new HashMap<>();
        map.put("bossAge",bossAge);
        map.put("empAge",empAge);
        return  map;
    }
}

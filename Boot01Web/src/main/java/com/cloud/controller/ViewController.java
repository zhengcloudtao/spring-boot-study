package com.cloud.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
@Controller
public class ViewController {
    @GetMapping("/cloud")
    public String cloud(Model model){
        //model中数据会被放在model中
        model.addAttribute("msg","你好cloud");
        model.addAttribute("url","http://www.meanman.cn");
        return "success";
    }
}

package com.cloud.boot04featuresprofile.bean;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class Color {
    @Profile("prod")
    @Bean
    public Color red(){
      return new Color();
    }

    @Profile("test")
    @Bean
    public Color green(){
        return new Color();
    }
}
